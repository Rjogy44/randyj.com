package mypackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/Contact")
public class Contact extends HttpServlet 
{
	
	String name;
	String email;
	String subject;
	String message1;
	private static final long serialVersionUID = 101821973209L;
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
            throws ServletException, IOException
  {
      
	  // Set response content type
//      response.setContentType("text/html");
	  response.sendRedirect("Success.jsp");
//      PrintWriter out = response.getWriter();
//	  String title = "Thank You!  I will respond as soon as Possible!";
//	  String hyperlink = "/Website/";
//      String docType =
//      "<!doctype html public \"-//w3c//dtd html 4.0 " +
//      "transitional//en\">\n";
//      out.println(docType +
//                "<html>\n" +
//                "<head><title>" + title + "</title></head>\n" +
//                "<body bgcolor=\"#f0f0f0\">\n" +
//                "<h1 align=\"center\">" + title + "</h1>\n" + 
//                "<p>Go <a href=" + hyperlink + ">Home</a></p>\n" +
//                "</body></html>");

    	if (request.getParameter("name") != null && request.getParameter("email") != null)
    	{
    		this.sendEmail();
    	}
    
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
	  System.out.print("Hello "); 
	  name = request.getParameter("name");
	  email = request.getParameter("email");
	  subject = request.getParameter("subject");
	  message1 = request.getParameter("message");
	  if (name != null && email != null)
      {
		  this.doGet(request, response);
      }
  }
  
  public void sendEmail()
  {
		final String username = "RandyJorgensen44@gmail.com";
		final String password = "Br@ndt#49";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("RandyJorgensen44@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse("RandyJorgensen44@gmail.com"));
			message.setSubject("Testing Subject");
			message.setText("Hey Randy,"
				+ "\n\n Please Contact " + name + " " + subject + " They would like to talk to you.  Their email number is " + email + " They also had the following message \n\n" + message1);

			Transport.send(message);

			System.out.println("Email sent!");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
  }

}

